﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace character
{
    public class Wizard : Character
    {
        public Wizard (int str, int agi, int inte, string name) : base(str, agi, inte, name)
        {
            characterClass = "Wizard";
            MaxHp = 150 + 10 * strength;
            hp = MaxHp;
            MaxMana = 200 + 10 * intellect;
            mana = MaxMana;
            armorRating = 1 + intellect;

        }

        public void Fireball(Character target)
        {
            target.hp -= 30 + intellect;
            mana -= 30;
            Console.WriteLine(this.Name + "shoots a fireball at " + target.Name);
        }

        public string Inspect()
        {
            return "wizard's stats";
        }

    }
}
