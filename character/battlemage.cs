﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace character
{
    public class Battlemage : Wizard
    {
        public int foci;
        public Battlemage(int str, int agi, int inte, string name) : base(str + 1 , agi + 1, inte + 3, name)
        {
            characterClass = "Battlemage";
            MaxHp = 200 + 10 * strength;
            hp = MaxHp;
            MaxMana = 250 + 10 * intellect;
            mana = MaxMana;
            armorRating = 8 + agility;
            foci = 5;
        }

        public void Implode(Character target)
        {
            target.hp -= 50 + intellect;
            mana -= 100;
            foci--;
            Console.WriteLine(this.Name + " collapses great force upon " + target.Name + "for " + 50 + intellect + "damage!");
        }
    }
}
