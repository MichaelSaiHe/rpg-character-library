﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace character
{
    public class Sharpshooter : Thief
    {
        public int arrows;
        public Boolean Stance { set; get; }
        public Sharpshooter(int str, int agi, int inte, string name) : base(str, agi + 3, inte + 1, name)
        {
            characterClass = "Sharpshooter";
            MaxHp = 175 + 10 * strength;
            hp = MaxHp;
            MaxMana = 225 + 10 * intellect;
            mana = MaxMana;
            armorRating = 8 + strength;
            arrows = 20;
        }

        public void Snipe(Character target)
        {
            if (Stance) { 
                target.hp -= 40 + agility;
                Console.WriteLine(this.Name + "snipes the weakpoint of " + target.Name);
            }
            else { 
                target.hp -= 10 + agility;
                Console.WriteLine(this.Name + "rapidly shoots an arrow at " + target.Name);
            }
            mana -= 40;
            arrows--;

        }


    }
}
