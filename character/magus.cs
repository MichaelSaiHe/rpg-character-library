﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace character
{
    public class Magus : Wizard
    {
        public Magus(int str, int agi, int inte, string name) : base(str, agi + 1, inte + 2, name)
        {
            characterClass = "Magus";
            MaxHp = 150 + 10 * strength;
            hp = MaxHp;
            MaxMana = 250 + 10 * intellect;
            mana = MaxMana;
            armorRating = 6 + strength;
        }

        public void Lightning(Character target)
        {
            target.hp -= 32 + intellect;
            mana -= 30;
            Console.WriteLine(this.Name + "shoots a bolt fo lightning at " + target.Name);
        }

    }
}
