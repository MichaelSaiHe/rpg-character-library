﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace character
{

    public abstract class Character
    {
        protected int strength;
        protected int agility;
        protected int intellect;
        public string characterClass;
        public string subClass;
        public int hp;
        public int MaxHp { get; set; }
        public int MaxMana { get; set; }
        public int mana;
        public int armorRating;
        public string Name { get; set; }

        public Character(int str, int agi, int inte, string name)
        {
            strength = str;
            agility = agi;
            intellect = inte;
            Name = name;
        }
        public void Attack(Character target)
        {
            target.hp -= 10 + strength;
            Console.WriteLine("You attack " + target.Name + "for " + 10 + strength + "damage");
        }

        public string Move(string direction)
        {
            return "Character moves in " + direction;
        }

        public int GetStr() { return strength; }
        public int GetAgi() { return agility; }
        public int GetInt() { return intellect; }
    }
}
