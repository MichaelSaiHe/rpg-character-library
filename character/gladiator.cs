﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace character
{
    public class Gladiator : Warrior
    {
        public Gladiator(int str, int agi, int inte, string name) : base(str+2, agi+1, inte, name)
        {
            characterClass = "Gladiator";
            MaxHp = 250 + 10 * strength;
            hp = MaxHp;
            MaxMana = 100 + 10 * intellect;
            mana = MaxMana;
            armorRating = 8 + strength;
        }

        public void Roar(Character target)
        {
            target.hp -= 20 + strength;
            mana -= 20;
            Console.WriteLine(this.Name + "roars at " + target.Name);
        }
    }
}
