﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace character
{
    public class Thief : Character
    {
        public Thief(int str, int agi, int inte, string name) : base(str, agi, inte, name)
        {
            characterClass = "Thief";
            MaxHp = 175 + 10 * strength;
            hp = MaxHp;
            MaxMana = 150 + 10 * intellect;
            mana = MaxMana;
            armorRating = 5 + agility;
        }

        public void CheapShot(Character target){
            target.hp -= 20 + agility;
            mana -= 20;
            Console.WriteLine(this.Name + " takes a cheapshot on " + target.Name);
        }
        public string Inspect()
        {
            return "thief's stats";
        }

    }
}
