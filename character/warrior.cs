﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace character
{
    public class Warrior : Character
    {
        public Warrior(int str, int agi, int inte, string name) : base(str, agi, inte, name)
        {
            characterClass = "Warrior";
            MaxHp = 200 + 10 * strength;
            hp = MaxHp;
            MaxMana = 100 + 10 * intellect;
            mana = MaxMana;
            armorRating = 10 + strength;
        }

        public void StronkAttack(Character target)
        {
            target.hp-= 15 + strength;
            mana -= 10;
            Console.WriteLine("Warrior uses StronkAttack on " + target.Name);
        }
        public string Inspect()
        {
            return "warrior's stats";
        }
    }
}
