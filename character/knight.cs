﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace character
{
    public class Knight : Warrior
    {
        public Knight (int str, int agi, int inte, string name) : base(str + 5, agi - 2, inte - 1, name)
        {
            characterClass = "Knight";
            MaxHp = 500 + 10 * strength;
            hp = MaxHp;
            MaxMana = 100 + 10 * intellect;
            mana = MaxMana;
            armorRating = 12 + strength;
        }

        public void Strike(Character target)
        {
            target.hp -= 25 + strength;
            mana -= 10;
            Console.WriteLine(this.Name + " skillfully strikes " + target.Name);
        }
    }
}
