## RPG Character Generator Library
Character library that was built into a DLL that was given to partner. 

### Character Types and Subtypes
* Wizard
* * Magus
* * Battlemage
* Thief
* * Sharpshooter
* * Ninja
* Warrior
* * Gladiator
* * Knight
* Doodad (for objects)

Each class and subclass has its own unique moves, as well as moves inherited from parents. 